# Projeto Hotel

Bem-vindo ao Projeto Hotel! Este projeto é um sistema de cadastro de hóspedes em Java.

## Pré-requisitos

Certifique-se de ter o seguinte instalado:

- Java Development Kit (JDK)
- Git
- Maven

## Como Iniciar

1. Clone o repositório:

   ```bash
   git clone https://gitlab.com/gconcli1/projetohotel.git
   cd projetohotel