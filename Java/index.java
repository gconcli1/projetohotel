package Java;
import java.util.Scanner;

public class index {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Sistema de Cadastro de Hóspedes");

        System.out.print("Nome: ");
        String nome = scanner.nextLine();

        System.out.print("Data de Nascimento: ");
        String dataNascimento = scanner.nextLine();

        System.out.print("CPF: ");
        String cpf = scanner.nextLine();

        System.out.print("CEP: ");
        String cep = scanner.nextLine();

        System.out.print("Cidade: ");
        String cidade = scanner.nextLine();

        System.out.print("Bairro: ");
        String bairro = scanner.nextLine();

        System.out.print("Número do Prédio: ");
        String numeroPredio = scanner.nextLine();

        System.out.print("Complemento (opcional): ");
        String complemento = scanner.nextLine();

        System.out.println("\nHóspede Cadastrado com Sucesso!");

        if (!complemento.isEmpty()) {
            System.out.println("Complemento: " + complemento);
        }

        scanner.close();
    }
}